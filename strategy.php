<?php

require_once 'vendor/autoload.php';

$orcamento = new \DP\Strategy\Orcamento(500);

$imposto = new \DP\Strategy\CalculadoraImpostos();

echo "ICMS";
s($imposto->calcula($orcamento,new \DP\Strategy\ICMS()));
echo "ISS:";
s($imposto->calcula($orcamento,new \DP\Strategy\ISS()));
echo "KCV";
s($imposto->calcula($orcamento,new \DP\Strategy\KCV()));