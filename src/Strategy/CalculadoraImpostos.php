<?php

namespace DP\Strategy;


class CalculadoraImpostos
{
    public function calcula(Orcamento $orcamento, ImpostoInterface $imposto)
    {
        return $imposto->calcula($orcamento);
    }
}