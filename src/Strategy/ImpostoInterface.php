<?php

namespace DP\Strategy;


interface ImpostoInterface
{
    public function calcula(Orcamento $orcamento);
}