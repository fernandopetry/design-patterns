<?php

namespace DP\Strategy;


class Orcamento
{
    private $valor;

    /**
     * Orcamento constructor.
     *
     * @param $valor
     */
    public function __construct($valor) { $this->valor = $valor; }


    public function getValor()
    {
        return $this->valor;
    }
}