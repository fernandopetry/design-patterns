<?php

namespace DP\Strategy;


class ISS implements ImpostoInterface
{

    public function calcula(Orcamento $orcamento)
    {
        return $orcamento->getValor() * 0.1;
    }
}